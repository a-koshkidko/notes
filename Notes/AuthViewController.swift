//
//  AutorizationViewController.swift
//  Notes
//
//  Created by Alexey Koshkidko on 8/9/19.
//  Copyright © 2019 Alexey Koshkidko. All rights reserved.
//

import UIKit
import WebKit
import CoreData

class AuthViewController: UIViewController {

    private let webView = WKWebView()
    //var context: NSManagedObjectContext!
    //var backgroundContext: NSManagedObjectContext!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Reachability.isConnectedToNetwork(){
            print("Internet Connection Available!")
            setupViews()
            var urlComponents = URLComponents(string: "https://github.com/login/oauth/authorize")
            
            urlComponents?.queryItems = [
                // URLQueryItem(name: "response_type", value: "token"),
                URLQueryItem(name: "client_id", value: "1c693849ffdf0ebe4386"),
                URLQueryItem(name: "scope", value: "gist, user")
            ]
            
            let url = urlComponents?.url
            let request = URLRequest(url: url!)
            webView.load(request)
            webView.navigationDelegate = self
        
        
        }else{
            print("Internet Connection not Available!")
//            var alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
//            alert.show()
        }
    }
    
    private func setupViews() {
        view.backgroundColor = .blue
        
        webView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(webView)
        NSLayoutConstraint.activate([
            webView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            webView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            webView.topAnchor.constraint(equalTo: view.topAnchor),
            webView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ])
    }
    @IBAction func localNotesBtnTap(_ sender: Any) {
        performSegue(withIdentifier: "auth", sender: self)
    }
}



extension AuthViewController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if let url = navigationAction.request.url {
            let targetString = url.absoluteString.replacingOccurrences(of: "#", with: "?")
            guard let components = URLComponents(string: targetString) else { return }
            
            if let code = components.queryItems?.first(where: { $0.name == "code" })?.value {
                print("code \(code)")

                var urlComponents = URLComponents(string: "https://github.com/login/oauth/access_token")
                urlComponents?.queryItems = [
                    URLQueryItem(name: "client_id", value: "1c693849ffdf0ebe4386"),
                    URLQueryItem(name: "client_secret", value: "b7a31f51d7d07442ca449e88be96cad9c24b4979"),
                    URLQueryItem(name: "code", value: code)
                ]

                let url = urlComponents?.url
                var request = URLRequest(url: url!)
                request.httpMethod = "POST"
                request.setValue("application/json", forHTTPHeaderField: "Accept")

                let semaphore = DispatchSemaphore(value: 0)
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in

                    guard let gotToken = try? JSONDecoder().decode(GithubAPI.self, from: data!) else {
                        print("Bad token. Please re-authorize")
                        self.navigationController?.pushViewController((self.navigationController?.topViewController)!, animated: true)
                        return
                    }
                    print("gottoken: \(gotToken.access_token)")
                    GithubAPI.instance.access_token = gotToken.access_token
                    semaphore.signal()
                }
                task.resume()
                semaphore.wait()
                
                performSegue(withIdentifier: "auth", sender: self)
            }
            dismiss(animated: true, completion: nil)
        }
            decisionHandler(.allow)
    }
}

private let scheme = "akgists" // схема для callback
