//
//  API.swift
//  Notes
//
//  Created by Alexey Koshkidko on 8/11/19.
//  Copyright © 2019 Alexey Koshkidko. All rights reserved.
//

import Foundation


class GithubAPI: Decodable {
    static let instance = GithubAPI()
    private init() {}
    
    var access_token: String = ""
    var gistFromBackdendID: String? = ""    
}
