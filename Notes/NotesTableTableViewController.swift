//
//  NotesTableTableViewController.swift
//  Notes
//
//  Created by Alexey Koshkidko on 7/21/19.
//  Copyright © 2019 Alexey Koshkidko. All rights reserved.
//

import UIKit
import WebKit
import CoreData

class NotesTableTableViewController: UITableViewController {
var fileNoteBook = FileNotebook(noteBookNotes: [Note]())
var currentNote = Note(NoteTitle: "", NoteContent: "", NoteImportance: Importance.normal)
private let cellIdentifier = "Cell"

var context: NSManagedObjectContext!
var backgroundContext: NSManagedObjectContext! {
        didSet {
            fetchData()
        }
    }
func fetchData() {

    let dbQLoad = OperationQueue()
    dbQLoad.qualityOfService = .userInteractive
    
    let dbBackLoad = OperationQueue()
    dbBackLoad.qualityOfService = .userInteractive
    
    let loadOperation = LoadNotesOperation(notebook: fileNoteBook, backendQueue: dbBackLoad, dbQueue: dbQLoad, context: backgroundContext)
    
    let loadQueue =  OperationQueue()
    loadQueue.qualityOfService = .userInteractive
    loadQueue.addOperation(loadOperation)
    loadQueue.waitUntilAllOperationsAreFinished()
    self.tableView.refreshControl?.endRefreshing()
    self.tableView.refreshControl = nil
    self.tableView.reloadData()
    }

    @IBOutlet var notesTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.beginRefreshing()
        
        context = CoreDataManager.instance.persistentContainer.viewContext
        backgroundContext = CoreDataManager.instance.persistentContainer.newBackgroundContext()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.add, target: self, action: #selector(createNote(_:)))
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Редактировать", style: .plain, target: self, action: #selector(editTable(_:)))

        NotificationCenter.default.addObserver(self, selector: #selector(managedObjectContextDidSave(notification:)), name: NSNotification.Name.NSManagedObjectContextDidSave, object: nil)
    }
    
    @objc func managedObjectContextDidSave(notification: Notification) {
        context.perform {
            self.context.mergeChanges(fromContextDidSave: notification)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tableView.reloadData()
    }
    
//    override func tabl
// MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fileNoteBook.Notes.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        let note = fileNoteBook.Notes[indexPath.row]
        
        guard let tableViewCell = cell as? NoteTableViewCell else { return cell}
        tableViewCell.setUp(with: note)

        return tableViewCell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        currentNote = fileNoteBook.Notes[indexPath.row]
        performSegue(withIdentifier: "editNote", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let dest = segue.destination as? NoteViewController {
            dest.noteToEdit = currentNote
            dest.fileNoteBookToEdit = fileNoteBook
        }
    }

    @objc func createNote(_ sender: Any) {
        currentNote = Note(NoteTitle: "", NoteContent: "", NoteImportance: Importance.normal)
        performSegue(withIdentifier: "editNote", sender: self)
    }
    
    @objc func editTable(_ sender: Any) {
        if self.tableView.isEditing {
            self.tableView.isEditing = false
        }
        else{
            self.tableView.isEditing = true
        }
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            
            //операция удаления
            let dbQRemove = OperationQueue()
            dbQRemove.qualityOfService = .userInteractive
            let dbBackSave = OperationQueue()
            dbBackSave.qualityOfService = .userInteractive
            
            let removeAndSaveToBackOperation = RemoveNoteOperation(note: fileNoteBook.Notes[indexPath.row], notebook: fileNoteBook, backendQueue: dbBackSave, dbQueue: dbQRemove, context: backgroundContext)
            OperationQueue.main.addOperation(removeAndSaveToBackOperation)
            
            self.tableView.reloadData()
        }
    }    
}


