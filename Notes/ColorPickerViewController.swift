
import UIKit

class ColorPickerViewController: UIViewController {
  @IBOutlet weak var colorPickerView: ColorPickerView! {
    didSet {
      colorPickerView.isHidden = false
    }
  }
  var delegate: ColorPickerDelegate?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    colorPickerView.isHidden = false
    colorPickerView.delegate = self
    }
}

// MARK: ColorPickerDelegate

extension ColorPickerViewController : ColorPickerDelegate {
  func doneButtonTapped() {
    navigationController?.popViewController(animated: true)
  }
  
  func colorPicked(sender: ColorPicker, color: UIColor) {
    delegate?.colorPicked(sender: sender, color: color)
  }
}

