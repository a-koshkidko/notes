//
//  NoteViewController.swift
//  Notes
//
//  Created by Alexey Koshkidko on 7/2/19.
//  Copyright © 2019 Alexey Koshkidko. All rights reserved.
//

import UIKit
import CoreData

class NoteViewController: UIViewController {
    let gradientLayer = CAGradientLayer()
    var noteToEdit = Note(NoteTitle: "", NoteContent: "", NoteImportance: Importance.normal)
    var fileNoteBookToEdit = FileNotebook(noteBookNotes: [Note]())
    var backgroundContext: NSManagedObjectContext!
    private var color = UIColor.white

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var noteName: UITextField!
    @IBOutlet weak var destroyDateSwitch: UISwitch!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    @IBOutlet weak var redPick: ColorSampleView! {
    didSet {
        if noteToEdit.color == UIColor.red {
            redPick.isMarked = true
        }
    }
    }
    @IBOutlet weak var blackPick: ColorSampleView! {
        didSet {
            if noteToEdit.color == UIColor.blue {
             blackPick.isMarked = true
            }
        }
    }
    @IBOutlet weak var greenPick: ColorSampleView! {
        didSet {
            if noteToEdit.color == UIColor.green {
                greenPick.isMarked = true
            }
        }
    }
    @IBOutlet weak var customPick: ColorSampleView!{
        didSet {
            customPick.setAsCustom()
            customPick.isMarked = true
        }
    }

    @IBOutlet var sampleViews: [ColorSampleView]!
    

    @IBAction func redTapped(_ sender: UITapGestureRecognizer) {
        textView.textColor = UIColor.red
        noteName.textColor = UIColor.red
        markView(redPick)
    }
    
    @IBAction func blackTapped(_ sender: UITapGestureRecognizer) {
        textView.textColor = UIColor.blue
        noteName.textColor = UIColor.blue
        markView(blackPick)
    }
    
    @IBAction func greenTapped(_ sender: UITapGestureRecognizer) {
        textView.textColor = UIColor.green
        noteName.textColor = UIColor.green
        markView(greenPick)
    }

    @IBAction func customLongPressed(_ sender: UILongPressGestureRecognizer){
        guard sender.state == .began else {return}
        performSegue(withIdentifier: "toColorPickerViewController", sender: self)
    }
    
    private func markView(_ markedView: ColorSampleView) {
        sampleViews.forEach { $0.isMarked = false }
        markedView.isMarked = true
        sampleViews.forEach { $0.setNeedsDisplay() }
        guard let markedColor = markedView.backgroundColor else {return}
        color = markedColor
    }
    
    @IBAction func destroyDateSwitched(_ sender: UISwitch) {
        datePicker.isHidden = !destroyDateSwitch.isOn
        }
    
    @IBAction func dateDestructionDateChanged(_ sender: Any) {
        noteToEdit.selfDestructionDate = datePicker.date
    }
    
    
    @objc private func keyboardWillShow(notification: NSNotification){
        guard let keyboardFrame = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        scrollView.contentInset.bottom = view.convert(keyboardFrame.cgRectValue, from: nil).size.height
    }

    @objc private func keyboardWillHide(notification: NSNotification){
        scrollView.contentInset.bottom = 0
    }

    private func registerNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    private func unregisterNotifications() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        scrollView.contentInset.bottom = 0
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        if isMovingFromParent {
                noteToEdit.title = noteName.text!
                noteToEdit.content = textView.text
                noteToEdit.color = textView.textColor!
                if !destroyDateSwitch.isOn {
                noteToEdit.selfDestructionDate = nil
                }
            
                    //операция сохранения
                    let dbQ = OperationQueue()
                    dbQ.qualityOfService = .userInteractive
                    let dbBack = OperationQueue()
                    dbBack.qualityOfService = .userInteractive

                    let saveOperation = SaveNoteOperation(note: noteToEdit, currentNotebook: fileNoteBookToEdit, backendQueue: dbBack, dbQueue: dbQ, context: backgroundContext)
            
                    let saveQueue =  OperationQueue()
                    saveQueue.qualityOfService = .userInteractive
                    saveQueue.addOperation(saveOperation)
                    saveQueue.waitUntilAllOperationsAreFinished()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        backgroundContext = CoreDataManager.instance.persistentContainer.newBackgroundContext()
        redPick.layer.borderWidth = 1
        blackPick.layer.borderWidth = 1
        greenPick.layer.borderWidth = 1
        customPick.layer.borderWidth = 1
        redPick.layer.borderColor = UIColor.black.cgColor
        blackPick.layer.borderColor = UIColor.black.cgColor
        greenPick.layer.borderColor = UIColor.black.cgColor
        customPick.layer.borderColor = UIColor.black.cgColor
    
        noteName.text = noteToEdit.title
        textView.text = noteToEdit.content
        noteName.textColor = noteToEdit.color
        textView.textColor = noteToEdit.color
        if let destrDate = noteToEdit.selfDestructionDate {
            destroyDateSwitch.isOn = true
            datePicker.isHidden = false
            datePicker.date = destrDate
        } else {
            destroyDateSwitch.isOn = false
            datePicker.isHidden = true
        }
    }
    override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            registerNotifications()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
            super.viewDidDisappear(animated)
            unregisterNotifications()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "toColorPickerViewController" else {return}
        let destination = segue.destination as? ColorPickerViewController
        destination?.delegate = self
    }
}

extension NoteViewController: ColorPickerDelegate {
    func doneButtonTapped() {
        print("TAPPED")
    }
    func colorPicked(sender: ColorPicker, color: UIColor) {
        customPick.backgroungImage.isHidden = true
        customPick.backgroundColor = color
        noteToEdit.color = color
        textView.textColor = color
        noteName.textColor = color
        markView(customPick)
    }
}
