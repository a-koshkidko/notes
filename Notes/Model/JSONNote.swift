//
//  jsonNote.swift
//  Notes
//
//  Created by Alexey Koshkidko on 8/4/19.
//  Copyright © 2019 Alexey Koshkidko. All rights reserved.
//

import Foundation
import UIKit

struct JSONNote: Codable {
    let uid: String
    var title: String
    var content: String
    var r: String?
    var g: String?
    var b: String?
    var a: String?
    let importance: Int?
    let selfDestructionDate: String?

    init(note: Note) {
        uid = note.uid
        title = note.title
        content = note.content
        r = note.color.rgba.red.description
        g = note.color.rgba.green.description
        b = note.color.rgba.blue.description
        a = note.color.rgba.alpha.description
        importance = note.importance.rawValue
        if note.selfDestructionDate != nil {
            let fmt = DateFormatter()
            fmt.dateFormat = "yyyy-MM-dd HH:mm:ss"
            selfDestructionDate = fmt.string(from: note.selfDestructionDate!)
        }
        else {
            selfDestructionDate = nil
        }
    }
}
