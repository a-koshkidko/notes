//
//  FileNotebook.swift
//  study
//
//  Created by Alexey Koshkidko on 6/30/19.
//  Copyright © 2019 Alexey Koshkidko. All rights reserved.
//

import Foundation

class FileNotebook {
    public var Notes = [Note]()
 
    init(noteBookNotes: [Note]) {
        Notes = noteBookNotes
    }
    
    public func add(_ note: Note) {
        if !self.Notes.contains(where: {note.uid == $0.uid}){
        self.Notes.append(note)
        }
        else{
            let index = self.Notes.firstIndex(where: {note.uid == $0.uid})
            self.Notes[index!] = note
        }
    }
        
    public func remove(with uid: String){
            self.Notes.removeAll(where: {uid == $0.uid})
        
    }
    public func saveToFile(){
        let path = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first!
        var jsonNotes: [[String: Any]] = []
        for note in self.Notes {
            jsonNotes.append(note.json)

        }
        do {
            let noteData = try JSONSerialization.data(withJSONObject: jsonNotes, options: [])
            FileManager.default.createFile(atPath: path.path, contents: noteData, attributes: nil)
        } catch {
            }
        
    }
    public func loadFromFile(){
        let path = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first!
        var isDir: ObjCBool = false
            if FileManager.default.fileExists(atPath: path.path, isDirectory: &isDir), isDir.boolValue {
                 do {
                    let noteData = FileManager.default.contents(atPath: path.path)
                    let noteArray: [[String: Any]] = try JSONSerialization.jsonObject(with: noteData!, options: []) as! [[String : Any]]
                    for note in noteArray {
                        if Note.parse(json: note) != nil{
                        self.add(Note.parse(json: note)!)
                        }
                    }
                }  catch{
                    }
            }
    }
}
