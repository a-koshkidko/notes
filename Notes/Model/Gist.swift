//
//  Gist.swift
//  Notes
//
//  Created by Alexey Koshkidko on 8/7/19.
//  Copyright © 2019 Alexey Koshkidko. All rights reserved.
//

import Foundation

struct Gist: Codable {
    let id: String?
    let files: [String: GistFile]
    let `public`: Bool
    let description: String?
}

struct GistFile: Codable {
    let filename: String?
    let content: String?
}
