//
//  NoteExtension.swift
//  study
//
//  Created by Alexey Koshkidko on 6/29/19.
//  Copyright © 2019 Alexey Koshkidko. All rights reserved.
//

import Foundation
import UIKit

extension Note{
    static func parse(json: [String: Any]) -> Note? {
        let noteID = json["uid"]
        let noteTitle = json["title"]
        let noteContent = json["content"]
        var noteColor = UIColor()
        if json["r"] != nil {
            noteColor = UIColor(red: json["r"] as! CGFloat, green: json["g"] as! CGFloat, blue: json["b"] as! CGFloat, alpha: json["a"] as! CGFloat)
        }
        
        var noteImportanceRaw: Int
        var noteImportance = Importance(rawValue: 2)
        if (json["importance"]) != nil {
            noteImportanceRaw = json["importance"] as! Int
            noteImportance = Importance.init(rawValue: noteImportanceRaw)
        }
        
        let fmt = DateFormatter()
        fmt.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let noteDestructionDate = fmt.date(from: (json["selfDestructionDate"] as! String))
        
        let note = self.init(NoteID: noteID as! String, NoteTitle: noteTitle as! String, NoteContent: noteContent as! String, NoteColor: noteColor, NoteImportance: noteImportance!, DestructionDate: noteDestructionDate)
        return note
    }
    
    var json: [String: Any] {
            get{
                var getJson = [String: Any]()
                getJson["uid"] = self.uid
                getJson["title"] = self.title
                getJson["content"] = self.content
               
                getJson["r"] = self.color.rgba.red
                getJson["g"] = self.color.rgba.green
                getJson["b"] = self.color.rgba.blue
                getJson["a"] = self.color.rgba.alpha
                
                getJson["importance"] = self.importance.rawValue

                if self.selfDestructionDate != nil {
                    let fmt = DateFormatter()
                    fmt.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    getJson["selfDestructionDate"] = fmt.string(from: self.selfDestructionDate!)
                }
                return getJson
        }
    }
}

extension UIColor {
    var rgba: (red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        self.getRed(&r, green: &g, blue: &b, alpha: &a)
        return (r, g, b, a)
    }
}

extension UIColor {
    convenience init(hex: Int, alpha: CGFloat = 1.0) {
        let red = CGFloat((hex & 0xFF0000) >> 16) / 255.0
        let green = CGFloat((hex & 0xFF00) >> 8) / 255.0
        let blue = CGFloat((hex & 0xFF)) / 255.0
        
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    convenience init(hex string: String, alpha: CGFloat = 1.0) {
        var hex = string.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if hex.hasPrefix("#") {
            let index = hex.index(hex.startIndex, offsetBy: 1)
            hex = String(hex[index...])
        }
        
        if hex.count < 3 {
            hex = "\(hex)\(hex)\(hex)"
        }
        
        if hex.range(of: "(^[0-9A-Fa-f]{6}$)|(^[0-9A-Fa-f]{3}$)", options: .regularExpression) != nil {
            if hex.count == 3 {
                
                let startIndex = hex.index(hex.startIndex, offsetBy: 1)
                let endIndex = hex.index(hex.startIndex, offsetBy: 2)
                
                let redHex = String(hex[..<startIndex])
                let greenHex = String(hex[startIndex..<endIndex])
                let blueHex = String(hex[endIndex...])
                
                hex = redHex + redHex + greenHex + greenHex + blueHex + blueHex
            }
            
            let startIndex = hex.index(hex.startIndex, offsetBy: 2)
            let endIndex = hex.index(hex.startIndex, offsetBy: 4)
            let redHex = String(hex[..<startIndex])
            let greenHex = String(hex[startIndex..<endIndex])
            let blueHex = String(hex[endIndex...])
            
            var redInt: CUnsignedInt = 0
            var greenInt: CUnsignedInt = 0
            var blueInt: CUnsignedInt = 0
            
            Scanner(string: redHex).scanHexInt32(&redInt)
            Scanner(string: greenHex).scanHexInt32(&greenInt)
            Scanner(string: blueHex).scanHexInt32(&blueInt)
            
            self.init(red: CGFloat(redInt) / 255.0,
                      green: CGFloat(greenInt) / 255.0,
                      blue: CGFloat(blueInt) / 255.0,
                      alpha: CGFloat(alpha))
        }
        else {
            self.init(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0)
        }
    }
    
    var hexValue: String {
        var color = self
        
        if color.cgColor.numberOfComponents < 4 {
            let c = color.cgColor.components!
            color = UIColor(red: c[0], green: c[0], blue: c[0], alpha: c[1])
        }
        if color.cgColor.colorSpace!.model != .rgb {
            return "#FFFFFF"
        }
        let c = color.cgColor.components!
        return String(format: "#%02X%02X%02X", Int(c[0]*255.0), Int(c[1]*255.0), Int(c[2]*255.0))
    }
}


