//
//  Note.swift
//  study
//
//  Created by Alexey Koshkidko on 6/27/19.
//  Copyright © 2019 Alexey Koshkidko. All rights reserved.
//

import Foundation
import UIKit

enum Importance: Int{
    case low = 1
    case normal = 2
    case high = 3
}

struct Note {
    let uid: String
    var title: String
    var content: String
    var color: UIColor
    let importance: Importance
    var selfDestructionDate: Date?
    
    init(NoteID: String = UUID().uuidString, NoteTitle: String, NoteContent: String, NoteColor: UIColor = UIColor.black, NoteImportance: Importance, DestructionDate: Date? = nil) {
        
        uid = NoteID
        title = NoteTitle
        content = NoteContent
        color = NoteColor
        importance = NoteImportance
        selfDestructionDate = DestructionDate
        }
    init(json: JSONNote) {

        uid = json.uid
        title = json.title
        content = json.content
        
        if json.r != nil {
            color = UIColor(red: CGFloat(Double(json.r!)!), green: CGFloat(Double(json.g!)!), blue: CGFloat(Double(json.b!)!), alpha: CGFloat(Double(json.a!)!))
        }
        else {
            color = UIColor.black
        }

        if json.importance != nil {
            importance = Importance.init(rawValue: json.importance!)!
        }
        else {
            importance = Importance.normal
        }
        
        let fmt = DateFormatter()
        fmt.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        if json.selfDestructionDate != nil {
            selfDestructionDate = fmt.date(from: (json.selfDestructionDate!))
        }
        else
        {
            selfDestructionDate = nil
        }
    }
    
    init(noteDB: NoteDB) {
        
        uid = noteDB.uid!
        title = noteDB.title!
        content = noteDB.content!
        
        if noteDB.r != nil {
            color = UIColor(red: CGFloat(Double(noteDB.r!)!), green: CGFloat(Double(noteDB.g!)!), blue: CGFloat(Double(noteDB.b!)!), alpha: CGFloat(Double(noteDB.a!)!))
        }
        else {
            color = UIColor.black
        }
        
        importance = Importance.init(rawValue: Int(noteDB.importance))!
        let fmt = DateFormatter()
        fmt.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        if noteDB.selfDestructionDate != nil {
            selfDestructionDate = noteDB.selfDestructionDate! as Date
        }
        else
        {
            selfDestructionDate = nil
        }
    }
}


