//
//  RemoveNoteDBOperation.swift
//  Notes
//
//  Created by Alexey Koshkidko on 7/28/19.
//  Copyright © 2019 Alexey Koshkidko. All rights reserved.
//

import Foundation
import CoreData

class RemoveNoteDBOperation: BaseDBOperation {
    private let note: Note
    var context: NSManagedObjectContext!
    
    init(note: Note,
         notebook: FileNotebook, contextToDeleteFrom: NSManagedObjectContext!) {
        self.note = note
        self.context = contextToDeleteFrom
        super.init(notebook: notebook)
    }
    
    override func main() {
        notebook.remove(with: note.uid)
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "NoteDB")
        fetch.predicate = NSPredicate(format: "uid = %@", "\(note.uid)")
        let request = NSBatchDeleteRequest(fetchRequest: fetch)
        do {
            try context.execute(request)
        } catch {
            print(error)
        }
                
        finish()
    }
}
