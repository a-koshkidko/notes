import Foundation
import CoreData

class SaveNoteDBOperation: BaseDBOperation {
    private let note: Note
    var result: [Note]?
    var backgroundContext: NSManagedObjectContext!
    
    init(note: Note,
         notebook: FileNotebook, contextToSave: NSManagedObjectContext! ) {
        self.note = note
        self.backgroundContext = contextToSave
        super.init(notebook: notebook)
    }
    
    override func main() {
        let request = NSFetchRequest<NoteDB>(entityName: "NoteDB")
        var notesFromDB = [NoteDB]()
        
        do {
            notesFromDB = try backgroundContext.fetch(request)
        } catch {
            print(error)
        }
        
        let semaphore = DispatchSemaphore(value: 0)
        if !notesFromDB.contains(where: {note.uid == $0.uid}){
            DispatchQueue.global(qos: .userInitiated).async { [weak self] in
                guard let `self` = self else { return }
                
                
                let noteToAdd = NoteDB(context: self.backgroundContext)
                noteToAdd.uid = self.note.uid
                noteToAdd.title = self.note.title
                noteToAdd.content = self.note.content
                noteToAdd.importance = Int64(self.note.importance.rawValue)
                noteToAdd.r = self.note.color.rgba.red.description
                noteToAdd.g = self.note.color.rgba.green.description
                noteToAdd.b = self.note.color.rgba.blue.description
                noteToAdd.a = self.note.color.rgba.alpha.description
                noteToAdd.selfDestructionDate = self.note.selfDestructionDate as NSDate?
                
                self.backgroundContext.performAndWait {
                    do {
                        try self.backgroundContext.save()
                        notesFromDB.append(noteToAdd)
                        semaphore.signal()
                    } catch {
                        print(error)
                    }
                }
            }
            
        }
        else{

            DispatchQueue.global(qos: .userInitiated).async { [weak self] in
                guard let `self` = self else { return }
                let noteToUpdate = notesFromDB.first(where: {self.note.uid == $0.uid})
                noteToUpdate!.setValue(self.note.title, forKey: "title")
                noteToUpdate!.setValue(self.note.content, forKey: "content")
                noteToUpdate!.setValue(self.note.importance.rawValue, forKey: "importance")
                noteToUpdate!.setValue(self.note.color.rgba.red.description, forKey: "r")
                noteToUpdate!.setValue(self.note.color.rgba.green.description, forKey: "g")
                noteToUpdate!.setValue(self.note.color.rgba.blue.description, forKey: "b")
                noteToUpdate!.setValue(self.note.color.rgba.alpha.description, forKey: "a")
                noteToUpdate!.setValue(self.note.selfDestructionDate, forKey: "selfDestructionDate")
            
            self.backgroundContext.performAndWait {
                do {
                    try self.backgroundContext.save()
                    semaphore.signal()
                } catch {
                    print(error)
                }
            }
            }
        }
        semaphore.wait()
        
        var gotFromDB = [Note]()
        for noteDB in notesFromDB {
            let note = Note(noteDB: noteDB)
            gotFromDB.append(note)
        }

        notebook.Notes = gotFromDB
        result = notebook.Notes
        print("notesFromDB: \(notesFromDB.count)")
        print("gotFromDB: \(gotFromDB.count)")
        finish()
    }
}
