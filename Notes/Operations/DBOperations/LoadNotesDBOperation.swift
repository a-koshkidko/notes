//
//  LoadNoteDBOperation.swift
//  Notes
//
//  Created by Alexey Koshkidko on 7/28/19.
//  Copyright © 2019 Alexey Koshkidko. All rights reserved.
//

import Foundation
import CoreData

class LoadNotesDBOperation: BaseDBOperation {
    
    var result: [Note]?
    var context: NSManagedObjectContext!
    
    init(notebook: FileNotebook, contextToLoad: NSManagedObjectContext! ) {
        super.init(notebook: notebook)
        self.context = contextToLoad
    }
    
    override func main() {
        let request = NSFetchRequest<NoteDB>(entityName: "NoteDB")
        var notesFromDB = [NoteDB]()
        
        do {
            notesFromDB = try context.fetch(request)
        } catch {
            print(error)
        }
        
        for noteDB in notesFromDB {
            let note = Note(noteDB: noteDB)
            notebook.add(note)
        }
        result = notebook.Notes
        finish()
    }
}
