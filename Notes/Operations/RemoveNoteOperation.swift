//
//  RemoveNoteOperation.swift
//  Notes
//
//  Created by Alexey Koshkidko on 7/28/19.
//  Copyright © 2019 Alexey Koshkidko. All rights reserved.
//

import Foundation
import CoreData

class RemoveNoteOperation: AsyncOperation {
    private let note: Note
    private let notebook: FileNotebook
    private let removeFromDb: RemoveNoteDBOperation
    private var saveToBackend: SaveNotesBackendOperation?
    
    private(set) var result: Bool? = false
    
    init(note: Note,
         notebook: FileNotebook,
         backendQueue: OperationQueue,
         dbQueue: OperationQueue,
         context: NSManagedObjectContext!) {
        self.note = note
        self.notebook = notebook
        
        removeFromDb = RemoveNoteDBOperation(note: note, notebook: notebook, contextToDeleteFrom: context)
        
        super.init()
        let semaphore = DispatchSemaphore(value: 0)
        
        addDependency(removeFromDb)
        dbQueue.addOperation(removeFromDb)
        removeFromDb.completionBlock = {
            semaphore.signal()
        }
        semaphore.wait()
        let saveToBackend = SaveNotesBackendOperation(notes: notebook.Notes)
        self.saveToBackend = saveToBackend
        self.addDependency(saveToBackend)
        backendQueue.addOperation(saveToBackend)
    }
    
    override func main() {
        switch saveToBackend!.result! {
        case .success:
            result = true
        case .failure:
            result = false
        }
        finish()
    }
}
