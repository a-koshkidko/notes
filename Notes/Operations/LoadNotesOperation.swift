//
//  LoadNotesOperation.swift
//  Notes
//
//  Created by Alexey Koshkidko on 7/28/19.
//  Copyright © 2019 Alexey Koshkidko. All rights reserved.
//

import Foundation
import CoreData

class LoadNotesOperation: AsyncOperation {
 
    private let notebook: FileNotebook
    private let loadFromDb: LoadNotesDBOperation
    private var loadFromBackend: LoadNotesBackendOperation?
    private var context: NSManagedObjectContext!
    
    private(set) var result: Bool? = false
    
    init(notebook: FileNotebook,
         backendQueue: OperationQueue,
         dbQueue: OperationQueue,
         context: NSManagedObjectContext!) {
        
        self.notebook = notebook
        self.context = context
        loadFromDb = LoadNotesDBOperation(notebook: notebook, contextToLoad: context)
        super.init()

        let loadFromBackend = LoadNotesBackendOperation(notes: notebook.Notes)
        self.loadFromBackend = loadFromBackend
        self.addDependency(loadFromBackend)
        backendQueue.addOperation(loadFromBackend)
        
        self.addDependency(loadFromDb)
        backendQueue.addOperation(loadFromDb)
    }
    
    override func main() {
        switch loadFromBackend!.result! {
        case .success(let notesFromBackend):
            result = true
            notebook.Notes = notesFromBackend
            
                DispatchQueue.global(qos: .userInitiated).async { [weak self] in
                    guard let `self` = self else { return }
                    let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "NoteDB")
                    let request = NSBatchDeleteRequest(fetchRequest: fetch)
                    do {
                        try self.context.execute(request)
                    } catch {
                        print(error)
                    }

                    for noteFromBackend in notesFromBackend {
                        let semaphore = DispatchSemaphore(value: 0)
                        if let noteToAdd: NoteDB = NSEntityDescription.insertNewObject(forEntityName: "NoteDB", into: self.context) as? NoteDB {
                        noteToAdd.uid = noteFromBackend.uid
                        noteToAdd.title = noteFromBackend.title
                        noteToAdd.content = noteFromBackend.content
                        noteToAdd.importance = Int64(noteFromBackend.importance.rawValue)
                        noteToAdd.r = noteFromBackend.color.rgba.red.description
                        noteToAdd.g = noteFromBackend.color.rgba.green.description
                        noteToAdd.b = noteFromBackend.color.rgba.blue.description
                        noteToAdd.a = noteFromBackend.color.rgba.alpha.description
                        noteToAdd.selfDestructionDate = noteFromBackend.selfDestructionDate as NSDate?
                        
                        self.context.performAndWait {
                            do {
                                try self.context.save()
                                    semaphore.signal()
                            } catch {
                                    print(error)
                                }
                            }
                        }
                        semaphore.wait()
                    }
                }
            
        case .failure:
            result = false
        }
        finish()
    }
}
