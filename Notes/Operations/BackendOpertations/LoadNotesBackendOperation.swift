//
//  LoadNotesBackendOperation.swift
//  Notes
//
//  Created by Alexey Koshkidko on 7/28/19.
//  Copyright © 2019 Alexey Koshkidko. All rights reserved.
//

import Foundation

enum LoadNetworkError {
    case unreachable
    case noData
}

enum LoadNotesBackendResult {
    case success([Note])
    case failure(LoadNetworkError)
}

class LoadNotesBackendOperation: BaseBackendOperation {
    var result: LoadNotesBackendResult?
    var backendNotes = [Note]()
    
    init(notes: [Note]) {
        super.init()
    }
    
    override func main() {
        
        let stringUrl = "https://api.github.com/gists"
        guard let url = URL(string: stringUrl) else { return }

        var requestGists = URLRequest(url: url)
        requestGists.setValue("token \(GithubAPI.instance.access_token)", forHTTPHeaderField: "Authorization")

        let semaphore = DispatchSemaphore(value: 0)

        URLSession.shared.dataTask(with: requestGists) { (data, response, error) in
            
            guard error == nil else {
                self.result = .failure(.unreachable)
                print("Error: \(error?.localizedDescription ?? "no description")")
                semaphore.signal()
                return
            }
            guard let data = data else {
                self.result = .failure(.noData)
                print("No data")
                semaphore.signal()
                return
            }

            guard let gistsFromBackend = try? JSONDecoder().decode([Gist].self, from: data) else {
                print("Error while parsing Gists")
                semaphore.signal()
                return
            }
            
            if let gistFromBackdend = gistsFromBackend.first(where:
                {$0.files["ios-course-notes-db"] != nil})
             {
                GithubAPI.instance.gistFromBackdendID = gistFromBackdend.id!
             }
            else {
                self.result = .failure(.noData)
                semaphore.signal()
                return
            }
            
            if let response = response as? HTTPURLResponse {
                
                switch response.statusCode {
                case 200..<300:
                    print("Successfully got gists")
                default:
                    self.result = .failure(.unreachable)
                    print("Status: \(response.statusCode)")
                }
            }

            semaphore.signal()
            
            }.resume()
        semaphore.wait()
        
        if !GithubAPI.instance.gistFromBackdendID!.isEmpty {
        let stringUrlNotes = "https://api.github.com/gists/\(GithubAPI.instance.gistFromBackdendID!)"
        guard let urlNotes = URL(string: stringUrlNotes) else { return }
        var requestNotes = URLRequest(url: urlNotes)
        requestNotes.setValue("token \(GithubAPI.instance.access_token)", forHTTPHeaderField: "Authorization")
        
        let semaphore2 = DispatchSemaphore(value: 0)
        URLSession.shared.dataTask(with: requestNotes) { (data, response, error) in
            
            guard error == nil else {
                self.result = .failure(.unreachable)
                print("Error: \(error?.localizedDescription ?? "no description")")
                semaphore2.signal()
                return
            }
            guard let data = data else {
                self.result = .failure(.noData)
                print("No data")
                semaphore2.signal()
                return
            }
            
            guard let notesGistFromBackend = try? JSONDecoder().decode(Gist.self, from: data) else {
                print("Error while parsing Gists")
                semaphore2.signal()
                return
            }
            
            if let fileFromBackend = notesGistFromBackend.files["ios-course-notes-db"] {
                let notesFromBackend =  try? JSONDecoder().decode([JSONNote].self, from: Data(fileFromBackend.content!.utf8))
                for JSONNote in notesFromBackend! {
                    let noteFromBackend = Note(json: JSONNote)
                    self.backendNotes.append(noteFromBackend)
                }
            }
            
            if let response = response as? HTTPURLResponse {
                
                switch response.statusCode {
                case 200..<300:
                    if self.backendNotes.isEmpty {
                        self.result = .failure(.unreachable)
                        print("No data")
                    }
                    else {
                        self.result = .success(self.backendNotes)
                        print("Successfully got notes from gist")
                    }
                    
                default:
                    self.result = .failure(.unreachable)
                    print("Status: \(response.statusCode)")
                }
            }
            
            semaphore2.signal()
            }.resume()
        semaphore2.wait()
        }
        finish()
    }
}
