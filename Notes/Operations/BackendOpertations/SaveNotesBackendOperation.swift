import Foundation

enum SaveNetworkError {
    case unreachable
    case fileNotFound
}

enum SaveNotesBackendResult {
    case success([Note])
    case failure(SaveNetworkError)
}

class SaveNotesBackendOperation: BaseBackendOperation {
    var result: SaveNotesBackendResult?
    var notesToSave = [Note]()
    
    init(notes: [Note]) {
        print("notes to save in back:  \(notes.count)")
        super.init()
    notesToSave = notes    
    }
    
    override func main() {

        var jsonNotes = [JSONNote]()

        for note in notesToSave {
            
            let jsonNote = JSONNote(note: note)
            jsonNotes.append(jsonNote)
        }
        
        var jsonGist = [String: Any]()
        let jsonEncoder = JSONEncoder()
        jsonEncoder.outputFormatting = .prettyPrinted
        let jsonData = try! jsonEncoder.encode(jsonNotes)
        
        
        //Convert to Data
        //do {
        //let jsonData = try JSONSerialization.data(withJSONObject: jsonNotes, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        //Convert back to string. Usually only do this for debugging
        if let JSONString = String(data: jsonData, encoding: String.Encoding.utf8) {
            jsonGist = [
                "public": true,
                "files": [
                    "ios-course-notes-db": [
                        "content": JSONString
                    ]
                ]
            ]
        }
        
        let valid = JSONSerialization.isValidJSONObject(jsonGist) // true
        print(valid)

        let stringUrl: String
        if GithubAPI.instance.gistFromBackdendID!.isEmpty {
            stringUrl = "https://api.github.com/gists"
        }
        else {
            stringUrl = "https://api.github.com/gists/\(GithubAPI.instance.gistFromBackdendID!)"
        }
        guard let url = URL(string: stringUrl) else { return }
        var request = URLRequest(url: url)
        if !GithubAPI.instance.gistFromBackdendID!.isEmpty {
            request.httpMethod = "PATCH"
        }
        else {
            request.httpMethod = "POST"
        }
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("token \(GithubAPI.instance.access_token)", forHTTPHeaderField: "Authorization")
        request.httpBody = try! JSONSerialization.data(withJSONObject: jsonGist)

        let semaphore = DispatchSemaphore(value: 0)
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            if request.httpMethod == "POST" {
                guard error == nil else {
                    self.result = .failure(.unreachable)
                    print("Error: \(error?.localizedDescription ?? "no description")")
                    semaphore.signal()
                    return
                    }
                guard let data = data else {
                    self.result = .failure(.unreachable)
                    print("No data")
                    semaphore.signal()
                    return
                    }
                guard let createdGist = try? JSONDecoder().decode(Gist.self, from:      data) else {
                        print("Error while parsing Gists")
                        return
                    }
                GithubAPI.instance.gistFromBackdendID = createdGist.id
            }
            if let response = response as? HTTPURLResponse {
                    switch response.statusCode {
                    case 200..<300:
                        self.result = .success(self.notesToSave)
                        print("Success \(response.statusCode)")
                        
                    default:
                        self.result = .failure(.unreachable)
                        print("Status: \(response.statusCode)")
                        print(GithubAPI.instance.access_token)
                    }
                }
            else {
                self.result = .failure(.unreachable)
            }

            semaphore.signal()

            }
         task.resume()
        
         semaphore.wait()
        
        finish()
    }
}
