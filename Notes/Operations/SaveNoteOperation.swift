import Foundation
import CoreData

class SaveNoteOperation: AsyncOperation {
    private let note: Note
    private let notebook: FileNotebook
    private let saveToDb: SaveNoteDBOperation
    private var saveToBackend: SaveNotesBackendOperation?
    
    private(set) var result: Bool? = false
    
    init(note: Note,
         currentNotebook: FileNotebook,
         backendQueue: OperationQueue,
         dbQueue: OperationQueue,
         context: NSManagedObjectContext!) {
        self.note = note
        self.notebook = currentNotebook
        
        saveToDb = SaveNoteDBOperation(note: note, notebook: self.notebook, contextToSave: context)

        super.init()
        let semaphore = DispatchSemaphore(value: 0)

        addDependency(saveToDb)
        dbQueue.addOperation(saveToDb)
        saveToDb.completionBlock = {
            semaphore.signal()
        }
            semaphore.wait()
            let saveToBackend = SaveNotesBackendOperation(notes: self.notebook.Notes)
            self.saveToBackend = saveToBackend
            self.addDependency(saveToBackend)
            backendQueue.addOperation(saveToBackend)
    }
    
    override func main() {
        switch saveToBackend!.result! {
        case .success:
            result = true
        case .failure:
            result = false
        }
        finish()
    }
}
