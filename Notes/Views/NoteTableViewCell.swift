//
//  NoteTableViewCell.swift
//  MyNotes
//
//  Created by Рома on 20/07/2019.
//  Copyright © 2019 Roman Kozlov. All rights reserved.
//

import UIKit

class NoteTableViewCell: UITableViewCell {
  
  @IBOutlet weak var title: UILabel!
  @IBOutlet weak var content: UILabel!
  @IBOutlet weak var tagView: UIView!
  
  
  override func awakeFromNib() {
    super.awakeFromNib()
    cleanUp()
  }
  override func prepareForReuse() {
    super.prepareForReuse()
    cleanUp()
  }

  func cleanUp() {
    title.text = ""
    content.text = ""
    tagView.backgroundColor = .white
  }
  
  func setUp(with note: Note) {
    title.text = note.title
    content.text = note.content
    tagView.backgroundColor = note.color
  }
  
}



