//
//  NoteDB+CoreDataProperties.swift
//  Notes
//
//  Created by Алексей on 18/08/2019.
//  Copyright © 2019 Alexey Koshkidko. All rights reserved.
//
//

import Foundation
import CoreData

extension NoteDB {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<NoteDB> {
        return NSFetchRequest<NoteDB>(entityName: "NoteDB")
    }

    @NSManaged public var a: String?
    @NSManaged public var b: String?
    @NSManaged public var content: String?
    @NSManaged public var g: String?
    @NSManaged public var importance: Int64
    @NSManaged public var r: String?
    @NSManaged public var selfDestructionDate: NSDate?
    @NSManaged public var title: String?
    @NSManaged public var uid: String?
    @NSManaged public var group: String?
}
